from isf.core import logger
import isf.hardware.ttltalker
import isf.hardware.avrloader
from .options import popular_baudrate
import string
import os
import time
import binascii #todo: check if needed
import re
import matplotlib.pyplot as plt
import numpy as np
from mpld3 import fig_to_html #TODO: add generator


class logicAnalyzer:
    curr_folder = ''

    def __init__(self):
        self.curr_folder = os.path.dirname(os.path.abspath(__file__))

    def __del__(self):
        pass

    def baudrate_detect(self, com_port="/dev/ttyUSB0", timeout=0.3,
                        str_hex="0d0a"):
        baud_dict = {}

        logger.info("Starting baudrate detection for {}".format(com_port))
        best_baud = 0
        best_baud_num = 0
        for baud in popular_baudrate:
            try:
                uart_obj = isf.hardware.ttltalker.TTLTalker(com_port, baud,
                                                            timeout)
                ans = uart_obj.send_bytes('', True, 100, str_hex)['ret_str']
                readable = sum(
                    [char in string.printable.encode() for char in ans])
                baud_dict[baud] = 0
                if len(ans) != 0:
                    baud_dict[baud] = readable * 1.0 / len(ans)
                if baud_dict[baud] > best_baud:
                    best_baud = baud_dict[baud]
                    best_baud_num = baud
                logger.debug(
                    str(baud) + ' -- ' + str(baud_dict[baud]))  # , str(ans))
                # TODO: will add after fix
                del uart_obj
            except Exception as e:
                # TODO: fix for high baudrate
                pass
        return {'best_baudrate': best_baud_num,
                'percents': round(best_baud * 100, 2)}

    def analog_reader(self, com_port="/dev/ttyUSB0", timeout=0.1,
                      channels="012345", monitoring_time=10,
                      open_graph=True, save_path="/tmp/1.svg"):
        loader_obj = isf.hardware.avrloader.AVRLoader()
        loader_obj.ino_uploader(
            self.curr_folder + '/arduino_code/analog_reader/analog_reader.ino',
            com_port,
            self.curr_folder + '/arduino_code/analog_reader/analog_reader.hex',
            10)
        logger.info("Please, wait for 5 seconds for arduino rebooting")
        time.sleep(5)
        uart_obj = isf.hardware.ttltalker.TTLTalker(com_port, 1000000,
                                                    timeout)

        logger.info("Starting to listen {} channels".format(channels))
        uart_obj.send_bytes('3d', False, 0, '0d0a')

        r = '([\d]+) ([\d]+) ([\d]+) ([\d]+) ([\d]+) ([\d]+)'
        coords = []
        time_arr = []

        start_time = time.time()
        while time.time() - start_time < monitoring_time:
            ans = uart_obj.read_output(100)['ret_str'].decode('charmap')
            str_arr = ans.split('\n')
            for x in str_arr:
                if bool(re.match(r, x)):
                    res = re.match(r, x)
                    time_arr.append(time.time() - start_time)
                    arr = res.groups()
                    logger.debug(str(arr))

                    filtered_arr = []
                    for c in channels:
                        filtered_arr.append(int(arr[int(c)]))
                    coords.append(filtered_arr)
        logger.info("Captured {} coordinated!".format(len(coords)))

        colors = ['r', 'g', 'b', 'y', 'b', 'c']

        for i in range(len(channels)):
            plt.plot(np.array(time_arr), np.array([x[i] for x in coords]),
                     colors[i])
        plt.savefig(save_path)
        logger.info('Image was saved to: {}'.format(save_path))
        if open_graph:
            plt.show()

    def digital_reader(self, com_port="/dev/ttyUSB0", timeout=0.1,
                       channels="0123456789abcdefghij", monitoring_time=10,
                       open_graph=True, save_path="/tmp/1.svg"):
        # TODO: dont work need to fix
        loader_obj = isf.hardware.avrloader.AVRLoader()
        loader_obj.ino_uploader(
            self.curr_folder + '/arduino_code/digital_reader/digital_reader.ino',
            com_port,
            self.curr_folder + '/arduino_code/digital_reader/digital_reader.hex',
            10)
        logger.info("Please, wait for 5 seconds for arduino rebooting")
        time.sleep(5)
        uart_obj = isf.hardware.ttltalker.TTLTalker(com_port, 1000000,
                                                    timeout)

        logger.info("Starting to listen {} channels".format(channels))
        uart_obj.send_bytes('3d', False, 0, '0d0a')

        r = ('([\d]+) ' * 20).strip()
        coords = []
        time_arr = []

        start_time = time.time()
        while time.time() - start_time < monitoring_time:
            ans = uart_obj.read_output(100)['ret_str'].decode('charmap')
            str_arr = ans.split('\n')
            for x in str_arr:
                if bool(re.match(r, x)):
                    res = re.match(r, x)
                    time_arr.append(time.time() - start_time)
                    arr = res.groups()
                    logger.debug(str(arr))

                    filtered_arr = []
                    for c in channels:
                        filtered_arr.append(int(arr[int(c, 20)]))
                    coords.append(filtered_arr)
        logger.info("Captured {} coordinated!".format(len(coords)))

        colors = ['r', 'g', 'b', 'y', 'b', 'c'] * 4

        for i in range(len(channels)):
            plt.plot(np.array(time_arr), np.array([x[i] for x in coords]),
                     colors[i])
        plt.savefig(save_path)
        logger.info('Image was saved to: {}'.format(save_path))
        if open_graph:
            plt.show()

    def software_uart(self, com_port="/dev/ttyUSB0", baudrate=115200, tx=5,
                      rx=6):

        logger.warning("It works unstable, be careful!")

        template_file = self.curr_folder + '/arduino_code/soft_uart/soft_uart.ino.tpl'
        ino_file = self.curr_folder + '/arduino_code/soft_uart/soft_uart.ino'

        f = open(template_file)
        tpl_content = f.read()
        tpl_content = tpl_content.replace('__RX__', str(rx))
        tpl_content = tpl_content.replace('__TX__', str(tx))
        tpl_content = tpl_content.replace('__BAUDRATE__', str(baudrate))
        f.close()
        f = open(ino_file, 'w')
        f.write(tpl_content)
        f.close()

        loader_obj = isf.hardware.avrloader.AVRLoader()
        loader_obj.ino_uploader(
            ino_file,
            com_port,
            self.curr_folder + '/arduino_code/soft_uart/soft_uart.hex',
            10)
        logger.info("Please, wait for 5 seconds for arduino rebooting")
        time.sleep(5)
        logger.info("Programming completed!")
