#include <SoftwareSerial.h>

SoftwareSerial mySerial(__RX__, __TX__);

void setup()
{
  Serial.begin(__BAUDRATE__);
  while (!Serial) {
  }
  mySerial.begin(__BAUDRATE__);
}

void loop() {
  if (mySerial.available())
    Serial.write(mySerial.read());
  if (Serial.available())
    mySerial.write(Serial.read());
}